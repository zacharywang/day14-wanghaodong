# ORID

## O

- code review
- AntDesign
- Integration with back-end
- Presentation

## R

full



## I

- Today's code review found that my code for modification and deletion of operations, are placed in a modal window, today the code refactoring, the different operations decoupled, to facilitate the subsequent development.
- Combined with what I learned in the previous days, I am now able to improve the ability of joint front-end and back-end development
-  Learned user journey, user story, elevator speech and MVP knowledge, this knowledge can help us better demand management, product design, master the core skills necessary for product managers, make better products!

## D

Want to apply this product thinking to future company work to design better, more customer-friendly systems