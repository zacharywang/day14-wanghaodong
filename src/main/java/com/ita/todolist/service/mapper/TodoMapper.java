package com.ita.todolist.service.mapper;

import com.ita.todolist.entity.Todo;
import com.ita.todolist.service.dto.TodoRequest;
import com.ita.todolist.service.dto.TodoResponse;
import org.springframework.beans.BeanUtils;

public class TodoMapper {
    public TodoMapper() {
    }

    public static Todo toEntity(TodoRequest request) {
        Todo todo = new Todo();
        BeanUtils.copyProperties(request, todo);
        todo.setName(request.getText());
        return todo;
    }

    public static TodoResponse toResponse(Todo todo) {
        TodoResponse response = new TodoResponse();
        BeanUtils.copyProperties(todo, response);
        response.setText(todo.getName());
        return response;
    }
}
