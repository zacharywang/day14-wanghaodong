package com.ita.todolist.service;

import com.ita.todolist.entity.Todo;
import com.ita.todolist.repository.TodoRepository;
import com.ita.todolist.service.dto.TodoRequest;
import com.ita.todolist.service.dto.TodoResponse;
import com.ita.todolist.service.mapper.TodoMapper;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class TodoService {
    private final TodoRepository todoRepository;

    public TodoService(TodoRepository todoRepository) {
        this.todoRepository = todoRepository;
    }

    public List<TodoResponse> findAll() {
        return todoRepository.findAll().stream().map(TodoMapper::toResponse).collect(Collectors.toList());
    }

    public TodoResponse create(TodoRequest todo) {
        return TodoMapper.toResponse(todoRepository.save(TodoMapper.toEntity(todo)));
    }

    public void delete(Long id) {
        todoRepository.deleteById(id);
    }

    public TodoResponse update(Long id, TodoRequest todoRequest) {
        Optional<Todo> optionalCompany = todoRepository.findById(id);
        Todo todo = TodoMapper.toEntity(todoRequest);
        optionalCompany.ifPresent(previousCompany -> {
            previousCompany.setName(todo.getName());
            previousCompany.setDone(todo.isDone());
        });
        return TodoMapper.toResponse(todoRepository.save(optionalCompany.get()));
    }

    public TodoResponse findById(Long id) {
        Todo todo = todoRepository.findById(id).orElseThrow(RuntimeException::new);
        return TodoMapper.toResponse(todo);
    }
}
