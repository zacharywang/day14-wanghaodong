package com.ita.todolist;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.ita.todolist.entity.Todo;
import com.ita.todolist.repository.TodoRepository;
import com.ita.todolist.service.TodoService;
import com.ita.todolist.service.dto.TodoRequest;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;


@SpringBootTest
@AutoConfigureMockMvc
class TodoControllerTest {

    @Autowired
    private MockMvc mockMvc;
    @Autowired
    private TodoRepository todoRepository;

    @BeforeEach
    void setUp() {
        todoRepository.deleteAll();
    }

    @Test
    void should_find_todos() throws Exception {
        Todo todo = new Todo();
        todo.setName("Study React");
        todoRepository.save(todo);
        mockMvc.perform(MockMvcRequestBuilders.get("/todo"))
                .andExpect(MockMvcResultMatchers.status().is(200))
                .andExpect(MockMvcResultMatchers.jsonPath("$.length()").value(1))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].id").isNotEmpty())
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].id").isNumber())
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].text").value(todo.getName()));
    }

    @Test
    void should_return_a_todo_when_findById_given_a_todo_id() throws Exception {
        Todo todo = new Todo();
        todo.setName("Study React");
        Todo savedTodo = todoRepository.save(todo);

        mockMvc.perform(MockMvcRequestBuilders.get("/todo/{id}", savedTodo.getId()))
                .andExpect(MockMvcResultMatchers.status().is(200))
                .andExpect(MockMvcResultMatchers.jsonPath("$.id").isNotEmpty())
                .andExpect(MockMvcResultMatchers.jsonPath("$.id").isNumber())
                .andExpect(MockMvcResultMatchers.jsonPath("$.id").value(savedTodo.getId()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.text").value("Study React"));
    }

    @Test
    void should_return_new_todo_when_create_given_a_todo() throws Exception {
        TodoRequest todoRequest = new TodoRequest();
        todoRequest.setText("whd");
        todoRequest.setDone(false);
        ObjectMapper objectMapper = new ObjectMapper();
        String todoRequestJson = objectMapper.writeValueAsString(todoRequest);

        mockMvc.perform(MockMvcRequestBuilders.post("/todo")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(todoRequestJson))
                .andExpect(MockMvcResultMatchers.status().is(201))
                .andExpect(MockMvcResultMatchers.jsonPath("$.id").isNumber())
                .andExpect(MockMvcResultMatchers.jsonPath("$.id").value(todoRepository.findAll().get(0).getId()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.text").value(todoRequest.getText()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.done").isBoolean())
                .andExpect(MockMvcResultMatchers.jsonPath("$.done").value(false));
    }

    @Test
    void should_delete_the_todo_when_delete_given_a_todo_id() throws Exception {
        Todo todo = new Todo();
        todo.setName("test del");
        Todo savedTodo = todoRepository.save(todo);

        mockMvc.perform(MockMvcRequestBuilders.delete("/todo/{id}", savedTodo.getId()))
                .andExpect(MockMvcResultMatchers.status().is(204));

        Assertions.assertTrue(
                todoRepository.findById(savedTodo.getId()).isEmpty()
        );
    }

    @Test
    void should_update_the_todo_when_update_given_a_new_todo_and_id() throws Exception {
        Todo todo = new Todo();
        todo.setName("test update");
        Todo savedTodo = todoRepository.save(todo);

        TodoRequest toUpdate = new TodoRequest();
        toUpdate.setText("updated!");
        toUpdate.setDone(true);

        ObjectMapper objectMapper = new ObjectMapper();
        String todoRequestJson = objectMapper.writeValueAsString(toUpdate);


        mockMvc.perform(MockMvcRequestBuilders.put("/todo/{id}", savedTodo.getId())
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(todoRequestJson))
                .andExpect(MockMvcResultMatchers.status().is(204))
                .andExpect(MockMvcResultMatchers.jsonPath("$.text").value(toUpdate.getText()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.done").value(toUpdate.isDone()));


    }

}
